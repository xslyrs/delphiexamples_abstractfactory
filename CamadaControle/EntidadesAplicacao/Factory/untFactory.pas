unit untFactory;

interface

uses
  intEntidadesNegocio;

type
 TFactory = class
  private
   FObjeto: InterfaceDaClasse;
  public
   constructor Iniciar;
   destructor Destroy; override;
   function CriarObjeto(pObj: TModelosDaFabrica): InterfaceDaClasse;

 end;

implementation

uses
  Vcl.Dialogs, untClasse1, untClasse2, untClasse3;

{ TFabrica }

constructor TFactory.Iniciar;
begin

end;

function TFactory.CriarObjeto(pObj: TModelosDaFabrica): InterfaceDaClasse;
begin
  Result := nil;

  case pObj of
    TipoClasse1: FObjeto := TClasse1.Create;
    TipoClasse2: FObjeto := TClasse2.Create;
    TipoClasse3: FObjeto := TClasse3.Create;
  end;
  Result := FObjeto;
end;

destructor TFactory.Destroy;
begin
  FObjeto._Release;
  inherited;
end;



end.
