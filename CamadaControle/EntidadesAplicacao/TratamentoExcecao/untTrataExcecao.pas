unit untTrataExcecao;

interface

uses
  System.SysUtils, Vcl.Graphics, Vcl.ExtCtrls;
type
 TTrataExcecao = class
  private
   procedure RegistraErroAplicacao(Sender: TObject; E: Exception);
   constructor Create;
end;
implementation

uses
  Vcl.Dialogs, Vcl.Forms, Winapi.Windows, System.Classes;

{ TTrataExcecao }


var
  vTratamentoDeExcecao: TTrataExcecao;

{ TTrataExcecao }

procedure TTrataExcecao.RegistraErroAplicacao(Sender: TObject; E: Exception);
begin
  showmessage('FFFFuuuuuuuuuuuuuuuuu:'+#13+e.Message);
end;

constructor TTrataExcecao.Create;
begin
  Application.OnException := RegistraErroAplicacao;
end;

initialization
  vTratamentoDeExcecao := TTrataExcecao.Create;

finalization
  vTratamentoDeExcecao.Free;

end.
