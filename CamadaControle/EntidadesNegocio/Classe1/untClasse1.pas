unit untClasse1;

interface

uses
  intEntidadesNegocio;
type
TClasse1 = class(TInterfacedObject, InterfaceDaClasse)
  public
  constructor Create;
  procedure MostrarMeuNome;
end;

implementation

uses
  Vcl.Dialogs;

{ TClasse1 }

constructor TClasse1.Create;
begin

end;

procedure TClasse1.MostrarMeuNome;
begin
  showmessage('classe1');
end;

end.
