unit untClasse2;

interface

uses
  intEntidadesNegocio;
type
TClasse2 = class(TInterfacedObject, InterfaceDaClasse)
  public
   constructor Create;
   procedure MostrarMeuNome;
 end;
implementation

uses
  Vcl.Dialogs;

{ TClasse2 }

constructor TClasse2.Create;
begin

end;

procedure TClasse2.MostrarMeuNome;
begin
  showmessage('Classe2');
end;

end.
