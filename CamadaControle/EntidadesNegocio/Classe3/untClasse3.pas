unit untClasse3;

interface

uses
  intEntidadesNegocio;
type
 TClasse3 = class(TInterfacedObject, InterfaceDaClasse)
   public
     constructor Create;
     procedure MostrarMeuNome;
 end;
implementation

uses
  Vcl.Dialogs;

{ TClasse3 }

constructor TClasse3.Create;
begin

end;

procedure TClasse3.MostrarMeuNome;
begin
  showmessage('Classe3');
end;

end.
