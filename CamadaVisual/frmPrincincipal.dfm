object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 417
  ClientWidth = 638
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 638
    Height = 30
    Align = alTop
    TabOrder = 0
    object GerarErro: TButton
      Left = 1
      Top = 1
      Width = 75
      Height = 28
      Align = alLeft
      Caption = 'GerarErro'
      TabOrder = 0
      OnClick = GerarErroClick
    end
    object btnGeraObj: TButton
      Left = 475
      Top = 1
      Width = 162
      Height = 28
      Align = alRight
      Caption = 'Gerar Objeto Via Factory'
      TabOrder = 1
      OnClick = btnGeraObjClick
    end
    object ComboBoxDeClasses: TComboBox
      AlignWithMargins = True
      Left = 393
      Top = 4
      Width = 79
      Height = 21
      Align = alRight
      TabOrder = 2
      Text = 'Classe1'
      Items.Strings = (
        'Classe1'
        'Classe2'
        'Classe3')
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 30
    Width = 638
    Height = 387
    Align = alClient
    TabOrder = 1
  end
end
