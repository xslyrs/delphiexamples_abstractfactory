unit frmPrincincipal;

interface

uses
  Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Graphics;

type
  TForm3 = class(TForm)
    GerarErro: TButton;
    Panel1: TPanel;
    btnGeraObj: TButton;
    ComboBoxDeClasses: TComboBox;
    Panel2: TPanel;

    procedure GerarErroClick(Sender: TObject);
    procedure btnGeraObjClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses
  Winapi.Windows, untFactory, intEntidadesNegocio;

{$R *.dfm}

procedure TForm3.btnGeraObjClick(Sender: TObject);
var
  FabricaDeObjetos :TFactory;
  ObjetoDesconhecido: InterfaceDaClasse;
begin

  FabricaDeObjetos := TFactory.Iniciar;

  case ComboBoxDeClasses.ItemIndex of
   0: ObjetoDesconhecido := FabricaDeObjetos.CriarObjeto(TipoClasse1);
   1: ObjetoDesconhecido := FabricaDeObjetos.CriarObjeto(TipoClasse2);
   2: ObjetoDesconhecido := FabricaDeObjetos.CriarObjeto(TipoClasse3);
  end;

  ObjetoDesconhecido.MostrarMeuNome;

  FabricaDeObjetos.Destroy;
end;

procedure TForm3.GerarErroClick(Sender: TObject);
var
 a,b:integer;
 c:double;
begin

  a:=1;
  b:=0;
  c:=a/b;

end;

end.
