program MeuProjetoExemplos;

uses
  Vcl.Forms,
  frmPrincincipal in 'CamadaVisual\frmPrincincipal.pas' {Form3},
  untTrataExcecao in 'CamadaControle\EntidadesAplicacao\TratamentoExcecao\untTrataExcecao.pas',
  untFactory in 'CamadaControle\EntidadesAplicacao\Factory\untFactory.pas',
  untClasse2 in 'CamadaControle\EntidadesNegocio\Classe2\untClasse2.pas',
  untClasse1 in 'CamadaControle\EntidadesNegocio\Classe1\untClasse1.pas',
  intEntidadesNegocio in 'CamadaControle\EntidadesNegocio\intEntidadesNegocio.pas',
  untClasse3 in 'CamadaControle\EntidadesNegocio\Classe3\untClasse3.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
